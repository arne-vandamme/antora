= Ordered and Unordered Lists
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// URLs
:url-adoc-manual: https://asciidoctor.org/docs/user-manual
:url-ordered: {url-adoc-manual}/#ordered-lists
:url-unordered: {url-adoc-manual}/#unordered-lists

On this page, you'll learn:

* [x] How to mark up an ordered list with AsciiDoc.
* [x] How to mark up an unordered list with AsciiDoc.

== Ordered list syntax

.Ordered list with nested levels
[source,asciidoc]
----
. Step 1
. Step 2
.. Details
... Mini-details
.... Micro-details
..... We're really down in the weeds now.
.. More details
. Step 3
----

The maximum nesting level is five (`pass:[.....]`).

. Step 1
. Step 2
.. Details
... Mini-details
.... Micro-details
..... We're really down in the weeds now.
.. More details
. Step 3

[discrete]
==== Asciidoctor resources

* {url-ordered}[Basic and complex ordered lists^]

[#unordered]
== Unordered list syntax

.Unordered list with nested levels
[source,asciidoc]
----
* Item A
* Item B
** Item B1
*** Details
**** More details
***** Details about the details
** Item B2
* Item C
----

The maximum nesting level is five (`pass:[*****]`).

* Item A
* Item B
** Item B1
*** Details
**** More details
***** Details about the details
** Item B2
* Item C

You'll use AsciiDoc's unordered list syntax to xref:navigation:list-structures.adoc[structure your site's navigation] as well.

[discrete]
==== Asciidoctor resources

* {url-unordered}[Basic and complex unordered lists^]
